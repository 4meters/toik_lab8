package com.demo.springboot.model;

public interface CheckIdentificationNumber {
    boolean checkIdentificationNumber(String id);
}
