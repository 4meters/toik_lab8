package com.demo.springboot.model;

public class CheckIdentificationNumberImpl implements CheckIdentificationNumber {
    @Override
    public boolean checkIdentificationNumber(String id) {
        if(id.length()==11){
            int[] checksum_array = {9, 7, 3, 1, 9, 7, 3, 1, 9, 7};
            int tmp_cyfra;
            int suma;
            int cyfra_kontrolna;
            int obl_cyfra_kontrolna;
            suma=0;
            for (int i = 0; i < 10; i++){
                tmp_cyfra = Integer.parseInt(id.substring(i,i+1));
                suma = suma + tmp_cyfra * checksum_array[i];
            }
            obl_cyfra_kontrolna=suma%10;
            cyfra_kontrolna=Integer.parseInt(id.substring(10,11));
            if(cyfra_kontrolna==obl_cyfra_kontrolna){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}
